<?php

namespace App\Repositories;

use App\Models\CatFact;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;

class BaseRepository
{
    /**
     * @var Model
     */
    protected Model $model;

    /**
     * BaseRepository constructor.
     *
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function getAllWithPagination(array $options = [], array $relations = []): ?LengthAwarePaginator
    {
        $pageSize = $options['pageSize'] ?? config('pagination.default_size');

        return $this->model->with($relations)->paginate($pageSize);
    }

    public function getById(
        int   $id,
        array $columns = ['*'],
        array $relations = [],
        array $appends = []
    ): Builder|array|Collection|Model
    {
        return $this->model->select($columns)->with($relations)->findOrFail($id)->append($appends);
    }

    public function store(array $options): Model
    {
        $model = $this->model;

        return $this->setFieldsAndPersist($model, $options);
    }

    public function update(Model $model, array $options): Model
    {
        return $this->setFieldsAndPersist($model, $options);
    }

    protected function setFieldsAndPersist(Model $model, array $options): ?Model
    {
        $model->fact = $options['fact'];

        if ($model->save()) {
            return $model;
        }

        return null;
    }

    public function destroy(Model $model): bool|null
    {
        return $model->delete();
    }
}
