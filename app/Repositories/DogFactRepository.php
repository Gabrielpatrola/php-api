<?php

namespace App\Repositories;

use App\Models\DogFact;
use Illuminate\Database\Eloquent\Model;

class DogFactRepository extends BaseRepository
{
    /**
     * @var Model
     */
    protected Model $model;

    /**
     * BaseRepository constructor.
     *
     * @param DogFact $model
     */
    public function __construct(DogFact $model)
    {
        $this->model = $model;
    }
}
