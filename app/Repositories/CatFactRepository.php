<?php
declare(strict_types=1);

namespace App\Repositories;

use App\Models\CatFact;
use Illuminate\Database\Eloquent\Model;

class CatFactRepository extends BaseRepository
{
    /**
     * @var Model
     */
    protected Model $model;

    /**
     * BaseRepository constructor.
     *
     * @param CatFact $model
     */
    public function __construct(CatFact $model)
    {
        $this->model = $model;
    }
}
