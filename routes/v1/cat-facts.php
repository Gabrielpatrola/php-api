<?php

use App\Http\Controllers\API\V1\CatFacts\Rest\DestroyController;
use App\Http\Controllers\API\V1\CatFacts\Rest\IndexController;
use App\Http\Controllers\API\V1\CatFacts\Rest\ShowController;
use App\Http\Controllers\API\V1\CatFacts\Rest\StoreController;
use App\Http\Controllers\API\V1\CatFacts\Rest\UpdateController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'cat-facts'], function () {
    // Standard RESTful routes using invokable controllers
    // More information on invokable controllers -> https://laravel.com/docs/9.x/controllers#single-action-controllers
    Route::get('{id}', ShowController::class)->name('api.v1.cat-facts.show');
    Route::put('{id}', UpdateController::class)->name('api.v1.cat-facts.update');
    Route::delete('{id}', DestroyController::class)->name('api.v1.cat-facts.destroy');

    Route::post('/', StoreController::class)->name('api.v1.cat-facts.store');
    Route::get('/', IndexController::class)->name('api.v1.cat-facts.index');
});
